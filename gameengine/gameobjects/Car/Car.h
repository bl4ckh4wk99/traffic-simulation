#pragma once

#include <time.h>
#include "../../AStar/AStar.h"
//#include "../Primitives/Mesh.h"
//#include "../../ObjectLoading/Texture.h"
#include "../Roads/Road.h"

enum Lane {
	CLEAR,
	CLOCKWISE,
	ANTICLOCKWISE,
	BOTH
};

static int currentID = 0;
class Car {
	
public:
	///Car ID
	int id;
	///Car Model and transform Matrix
	AssimpModel* model;
	Transform transform;

	///Car world and cell positions
	glm::vec3 worldPos;
	glm::vec2 cellPos;

	vector<glm::vec2> path;
	float vel;
	Direction direction;

	int* gridSize;
	int* tileSize;
	float timeStatic;
	glm::vec3 moveVec;

public:

	///Default Constructor
	Car() {}
	
	///Implicit Constructor
	Car(glm::vec2 cellPos, glm::vec3 worldPos, Direction dir, AssimpModel* model, int* gridSize, int* tileSize);

	///Initilise a Car object
	void Init(int* roadMap, vector<Direction>** carMap);

	///Draw a Car object
	void Draw(Shader* carShader, Camera cam);

	///Update a Car Object
	void Update(int* roadMap, Road*** roads, vector<Direction>** carMap, double deltaT);

	///Process the A* path of this Car Object
	void processPath();

	///Generate a new A* path for this Car Object
	void generateNewPath(int* roadMap, vector<Direction>** carMap);

	///Calculate the movement for this Car Object
	glm::vec3 calcMovement(Road*** roads, vector<Direction>** carMap, double deltaT);

	///Handle traffic junctions for this Car Object
	void trafficJunctionControl(glm::vec3 moveVec, glm::vec3 targetPos, Road* currentRoad, Road* nextRoad, std::pair <int, int> nxt, vector<Direction>** carMap, Direction entryDir, double deltaT, Road*** roads);

	///Move this Car Object
	void moveCar(glm::vec3 moveVec, vector<Direction>** carMap, Road*** roads);

};
