#pragma once

#include "RoadDetails.h"
#include <vector>
#include <queue>
#include "../GameObjects/Transform.h"
#include "../Shader/Shader.h"
#include "../GameObjects/Camera.h"
#include "../OpenGLHelper.h"
#include "../../ObjectLoading/Assimp/AssimpModel.h"


enum RoadType { EMPTY = 1, STRAIGHT = 2, CORNER = 3, TJUNC = 4, CROSSROADS = 5 };

class Road {
public:
	///Default Road Constructor
	Road() {}

	///Virtual rotate road function
	virtual void rotate() {};


	///Draw this Road Object
	void Draw(Shader* roadShader, Camera cam);
	///Draw this Road Object using heatmaps
	void DrawHeatMap(Shader* heatmapShader, Camera cam, int simTime);
	///Update this Road Object using deltaT
	virtual void Update(double deltaT) {};

	///Set the model of this Road Object
	void setModel(AssimpModel** newModel);

	///Methods for getting and returing the Stats of this Road Object
	string toString(int simTime);
	string getRoadType();
	string getMostActiveLight();

	///This Road Objects positional Data
	RoadType roadType;
	glm::vec2 cellPos;
	glm::vec3 worldPos;

	///vector that contains details on all entrances and exits from this road tile
	std::vector<RoadDetails*> roadLinks;

	///queue structure to store traffic data for traffic info
	std::queue<Direction> activeTraffic;
	int lights[4];

	float timeOccupiedByCar;
	int timesTraversed;
	int isSelected;
protected:
	///Model and Transform matrix of this Road Object
	AssimpModel** model;
	Transform transform;
	

	
};