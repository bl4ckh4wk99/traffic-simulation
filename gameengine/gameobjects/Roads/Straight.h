#pragma once
#include "Road.h"

class Straight : public Road {

public:
	///Straight Road piece constructor
	Straight(glm::vec2 cellPos, glm::vec3 worldPos, AssimpModel** model);
	///Update Straight Object Method using deltaT
	void Update(double deltaT);
	///Override the virtual rotate function
	void rotate();

};
