#pragma once

#include "Road.h"

class Crossroad : public Road {
private:
	///Traffic light data
	float maxGreenTime;
	float timeSinceGreen;

public:
	///Crossroad Road piece constructor
	Crossroad(glm::vec2 cellPos, glm::vec3 worldPos, AssimpModel** model);
	///Update Crossroad Object Method using deltaT
	void Update(double deltaT);
	///Override the virtual rotate function
	void rotate();
};
