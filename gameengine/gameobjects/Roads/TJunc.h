#pragma once

#include "Road.h"

class TJunc : public Road {
private:
	///Traffic light data
	float maxGreenTime;
	float timeSinceGreen;

public:
	///TJunc Road piece constructor
	TJunc(glm::vec2 cellPos, glm::vec3 worldPos, AssimpModel** model);
	///Update TJunc Object Method using deltaT
	void Update(double deltaT);
	///Override the virtual rotate function
	void rotate();
};
