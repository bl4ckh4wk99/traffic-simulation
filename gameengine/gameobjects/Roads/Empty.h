#pragma once
#include "Road.h"

class Empty : public Road {
public:
	///Empty Road piece constructor
	Empty(glm::vec2 cellPos, glm::vec3 worldPos, AssimpModel** model);
	///Update Empty Object Method using deltaT
	void Update(double deltaT);
	///Override the virtual rotate function
	void rotate();

};