#pragma once

#include "Road.h"

class Corner : public Road {
public:
	///Corner Road piece constructor
	Corner(glm::vec2 cellPos, glm::vec3 worldPos, AssimpModel** model);

	///Update Corner Object Method using deltaT
	void Update(double deltaT);

	///Override the virtual rotate function
	void rotate();
};